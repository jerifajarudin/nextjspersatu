import { useRouter } from "next/router"
import Layout from "../../components/Layout";

export default function Details() {
  const router = useRouter();
  const {id} = router.query;
  return (
    <Layout pageTitle="Detail Users">
      <p className="title">
        Page Details {id}
      </p>
    </Layout>
  )
}
