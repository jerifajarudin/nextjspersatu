import Layout from "../../components/Layout";

interface UserProps {
  dataUsers: Array<any>;
}
export default function Users(props: UserProps) {
  const {dataUsers} = props;
  return (
    <Layout pageTitle="Page Users">
      <p>View Users</p>
      {dataUsers.map((user) =>(
        <>
          <p>{user.id}</p>
          <p>{user.name}</p>
          <p>{user.email}</p>
        </>
      ))}
    </Layout>
  )
}

export async function getStaticProps() {
  const res = await fetch('https://jsonplaceholder.typicode.com/users');
  const dataUsers = await res.json();
  return {
    props: {
      dataUsers,
    },
  };
}
